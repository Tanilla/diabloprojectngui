﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Test : MonoBehaviour
{
    public string HeroName;
    public Class HeroClass;
    public bool IsMale;

    [ContextMenu("SetNewHero")]
    public void SetNewHero()
    {
        MainGameInstance.GameInstance.SetNewHero(new Hero(HeroName, IsMale, HeroClass));
    }
}
