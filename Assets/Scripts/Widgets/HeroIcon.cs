﻿using System;
using UnityEngine;
using System.Collections;

public class HeroIcon : MonoBehaviour
{
    [SerializeField] private UILabel _nameLabel;
    [SerializeField] private UISprite _iconSprite;
    [SerializeField] private UISprite _borderSprite;
    [SerializeField] private UILabel _levelLabel;

    protected void OnEnable()
    {
        if(MainGameInstance.GameInstance.CurrentHero != null)
            SetNewHero(MainGameInstance.GameInstance.CurrentHero);
        MainGameInstance.GameInstance.CurrentHeroChanged += SetNewHero;
    }

    protected void OnDisable()
    {
        MainGameInstance.GameInstance.CurrentHeroChanged -= SetNewHero;
    }

    private void SetNewHero(Hero currentHero)
    {
        _nameLabel.text = currentHero.Name;
        _levelLabel.text = currentHero.Level.ToString();
        SetHeroIcon(currentHero.Class, currentHero.Sex);
    }

    private void SetHeroIcon(Class heroClass, bool sex)
    {
        switch (heroClass)
        {
            case Class.Barbarian:
                _iconSprite.spriteName = sex ? "Portrait_Barbarian_Male" : "Portrait_Barbarian_Female";
                break;
            case Class.Crusader:
                _iconSprite.spriteName = sex ? "Portrait_Crusader_Male" : "Portrait_Crusader_Female";
                break;
            case Class.DemonHunter:
                _iconSprite.spriteName = sex ? "Portrait_Demonhunter_Male" : "Portrait_Demonhunter_Female";
                break;
            case Class.Monk:
                _iconSprite.spriteName = sex ? "Portrait_Monk_Male" : "Portrait_Monk_Female";
                break;
            case Class.WitchDoctor:
                _iconSprite.spriteName = sex ? "Portrait_Witchdoctor_Male" : "Portrait_Witchdoctor_Female";
                break;
            case Class.Wizard:
                _iconSprite.spriteName = sex ? "Portrait_Wizard_Male" : "Portrait_Wizard_Female";
                break;
        }
    }
}
