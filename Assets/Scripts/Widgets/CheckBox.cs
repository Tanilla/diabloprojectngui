﻿using UnityEngine;
using System.Collections;

public class CheckBox : MonoBehaviour
{
    [SerializeField] private bool _state;
    [SerializeField] private Sprite _selectedSprite;
    [SerializeField] private Sprite _baseSprite;
}
