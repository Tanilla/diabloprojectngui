﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenManager
{
    private Dictionary<Screens, BaseScreen> _loadedScreens;
    private BaseScreen _currentScreen;
    public ScreenManager()
    {
        _loadedScreens = new Dictionary<Screens, BaseScreen>();
    }

    public BaseScreen ShowScreen(Screens screenType)
    {
        BaseScreen screen;
        if (!_loadedScreens.ContainsKey(screenType))
        {
            screen = LoadScreen(screenType);
            UICamera.current.gameObject.AddChild(screen.gameObject);
            _loadedScreens[screenType] = screen;
        }
        screen = _loadedScreens[screenType];
        ShowScreen(screen);
        return screen;
    }

    private void ShowScreen(BaseScreen screen)
    {
        if(_currentScreen != null) _currentScreen.Close();
        _currentScreen = screen;
        _currentScreen.Show();
    }

    public void CloseScreen(Screens screenType)
    {
        if (!_loadedScreens.ContainsKey(screenType)) return;
        _loadedScreens[screenType].Close();
    }

    public BaseScreen LoadScreen(Screens screenType)
    {
        switch (screenType)
        {
            case Screens.Login:
                break;
            case Screens.Lobby:
                break;
            case Screens.HeroSelection:
                break;
            case Screens.Game:
                break;
        }
        return new LobbyScreen();
    }
}
