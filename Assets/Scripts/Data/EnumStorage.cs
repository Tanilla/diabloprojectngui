﻿using System.ComponentModel;

public enum Class
{
    [Description("Barbarian")]
    Barbarian,
    [Description("Crusader")]
    Crusader,
    [Description("Demon Hunter")]
    DemonHunter,
    [Description("Monk")]
    Monk,
    [Description("Witch Doctor")]
    WitchDoctor,
    [Description("Wizard")]
    Wizard
}

public enum Screens
{
    Login,
    Lobby, 
    HeroSelection,
    Game
}
