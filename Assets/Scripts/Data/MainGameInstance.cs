﻿using System;
using UnityEngine;
using System.Collections;
using JetBrains.Annotations;

public class MainGameInstance
{
    public Action<Hero> CurrentHeroChanged;

    private static MainGameInstance _gameInstance;

    public static MainGameInstance GameInstance
    {
        get
        {
            if(_gameInstance == null)
                _gameInstance = new MainGameInstance();
            return _gameInstance;
        }
        set { _gameInstance = value; }
    }

    private Hero _currentHero;
    public Hero CurrentHero
    {
        get { return _currentHero; }
        private set
        {
            if (value == _currentHero) return;
            _currentHero = value;
            if (CurrentHeroChanged != null)
                CurrentHeroChanged(_currentHero);
        }
    }

    public void SetNewHero(Hero hero)
    {
        CurrentHero = hero;
    }
}
