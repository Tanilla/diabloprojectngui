﻿using System;

public class Hero
{
    public Action<string> HeroNameChanged;
    public Action<int> HeroLevelChanged;
    public Action<int> HeroExperienceChanged;   

    private string _name;

    public string Name
    {
        get { return _name; }
        private set
        {
            _name = value;
            if (HeroNameChanged != null)
                HeroNameChanged(_name);
        }
    }

    private int _level;

    public int Level
    {
        get { return _level; }
        private set
        {
            _level = value;
            if (HeroLevelChanged != null)
                HeroLevelChanged(_level);
        }
    }

    private int _experience;

    public int Experience
    {
        get { return _experience;}
        private set
        {
            _experience = value;
            if (HeroExperienceChanged != null)
                HeroExperienceChanged(_experience);
        }
    }

    public bool Sex { get; private set; } // 0 is Female, 1 is Male

    public Class Class { get; private set; }


    public Hero(string name, bool sex, Class heroClass)
    {
        _name = name;
        Class = heroClass;
        Level = 1;
        Experience = 0;
        Sex = sex;
    }

    public void AddExperience(int expAmount)
    {
        //temp code for leveling
        if (Experience + expAmount > 100)
        {
            Experience = Experience + expAmount - 100;
            Level++;
        }
        else
        {
            Experience += expAmount;
        }
    }

}
