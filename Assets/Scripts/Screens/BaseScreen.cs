﻿using UnityEngine;
using System.Collections;

public class BaseScreen : MonoBehaviour, ScreenInterface
{
    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Close()
    {
        gameObject.SetActive(false);
    }
}
