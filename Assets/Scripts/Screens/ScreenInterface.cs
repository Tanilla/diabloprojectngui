﻿using UnityEngine;
using System.Collections;

public interface ScreenInterface
{
    void Show();
    void Close();
}
