﻿using UnityEngine;
using System.Collections;

public class LobbyScreen : BaseScreen
{
    [SerializeField] private UILabel _compaignNameLabel;
    [SerializeField] private UILabel _tormentLabel;
    [SerializeField] private UIButton _resumeGameButton;
    [SerializeField] private UIButton _gameSettingsButton;
    [SerializeField] private UIButton _achievementsButton;
    [SerializeField] private UIButton _profileButton;

    private EventDelegate _resumeGameEventDelegate;
    private EventDelegate _gameSettingsEventDelegate;
    private EventDelegate _achievementsEventDelegate;
    private EventDelegate _profileEventDelegate;

    public override void Show()
    {
        base.Show();
        Subscribe();
    }

    public override void Close()
    {
        Unsubscribe();
        base.Close();
    }

    private void Subscribe()
    {
        if(_resumeGameEventDelegate == null) _resumeGameEventDelegate = new EventDelegate(ResumeGame);
        _resumeGameButton.onClick.Add(_resumeGameEventDelegate);

        if(_gameSettingsEventDelegate == null) _gameSettingsEventDelegate = new EventDelegate(ShowGameSettings);
        _gameSettingsButton.onClick.Add(_gameSettingsEventDelegate);

        if(_achievementsEventDelegate == null) _achievementsEventDelegate = new EventDelegate(ShowAchievements);
        _achievementsButton.onClick.Add(_achievementsEventDelegate);

        if(_profileEventDelegate == null) _profileEventDelegate = new EventDelegate(ShowProfile);
        _profileButton.onClick.Add(_profileEventDelegate);
    }

    private void Unsubscribe()
    {
        if (_resumeGameEventDelegate != null && _resumeGameButton.onClick.Contains(_resumeGameEventDelegate))
            _resumeGameButton.onClick.Remove(_resumeGameEventDelegate);
        if (_gameSettingsEventDelegate != null && _gameSettingsButton.onClick.Contains(_gameSettingsEventDelegate))
            _gameSettingsButton.onClick.Remove(_gameSettingsEventDelegate);
        if (_achievementsEventDelegate != null && _achievementsButton.onClick.Contains(_achievementsEventDelegate))
            _achievementsButton.onClick.Remove(_achievementsEventDelegate);
        if (_profileEventDelegate != null && _profileButton.onClick.Contains(_profileEventDelegate))
            _profileButton.onClick.Remove(_profileEventDelegate);
    }

    private void ResumeGame()
    {
        
    }

    private void ShowGameSettings()
    {
        
    }

    private void ShowAchievements()
    {
        
    }

    private void ShowProfile()
    {
        
    }

}
